package snake;

import javax.swing.*;

import snake.Campo.Tela_do_Jogo;

public class Big extends Simple {
	@Override
    protected void CarregarImagens(){
        ImageIcon imagem = new ImageIcon("src/snake/big.png");
        setImage(imagem.getImage());
    }
	
	public void Comido(Tela_do_Jogo tela_do_jogo, Comum cobra) {
		cobra.Placar();
		cobra.Placar();
		cobra.Crescer();
	}
}
