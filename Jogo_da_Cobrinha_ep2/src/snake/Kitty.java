package snake;

import javax.swing.*;

public class Kitty extends Comum {

	@Override
    protected void CarregarImagens() {
        ImageIcon image = new ImageIcon("src/snake/kitty.png");
        this.setCorpo(image.getImage());
    }
	
	@Override
    public boolean Colisao_Barreira(Barreira barreira1, Barreira barreira2, Barreira barreira3) {
        return true;
    }
	
}
