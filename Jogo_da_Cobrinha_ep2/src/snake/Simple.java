package snake;

import javax.swing.*;

import snake.Campo.Tela_do_Jogo;

import java.awt.*;

public class Simple {
	
	protected final int tamanho_da_frutinha = 10;
	protected int posicao_aleatoria_frutinha = 29;
	protected int frutinhas_x;
	protected int frutinhas_y;
	protected int x;
	protected int y;
	
	private Image imagem;
	
	public Simple(){
		localizar_comida();
		CarregarImagens();
	}
	
	public void localizar_comida() {
		int b = (int) (Math.random() * posicao_aleatoria_frutinha);
		setX(b * tamanho_da_frutinha);
		
		b = (int) (Math.random() * posicao_aleatoria_frutinha);
		setY(b * tamanho_da_frutinha);
	}
	
	public Image getImage() {
        return imagem;
    }

    public void setImage(Image imagem) {
        this.imagem = imagem;
    }
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	protected void CarregarImagens(){
        ImageIcon imagem = new ImageIcon("src/snake/simple.png");
        setImage(imagem.getImage());
    }
	
	public void Comido(Tela_do_Jogo tela_do_Jogo, Comum cobra) {
		cobra.Placar();
		cobra.Crescer();
	}
	
	public boolean na_Barreira(Barreira barreira1, Barreira barreira2, Barreira barreira3) {
        for (int i = 0; i < 25; i++) {
            if (this.getX() == barreira1.getX()[i] && this.getY() == barreira1.getY()[i]) {
                return true;
            }
        }
        for (int i = 0; i < 25; i++) {
            if (this.getX() == barreira2.getX()[i] && this.getY() == barreira2.getY()[i]) {
                return true;
            }
        }
        for (int i = 0; i < 10; i++) {
            if (this.getX() == barreira3.getX()[i] && this.getY() == barreira3.getY()[i]) {
                return true;
            }
        }
        return false;
    }
}