package snake;

import snake.Campo.Tela_do_Jogo;

import javax.swing.*;

public class Bomb extends Simple {
	 @Override
	    protected void CarregarImagens() {
	        ImageIcon imagem = new ImageIcon("src/snake/bomb.png");
	        this.setImage(imagem.getImage());
	 }
	
	@Override
	public void Comido(Tela_do_Jogo tela_do_jogo, Comum cobra) {
        tela_do_jogo.no_jogo = false;
    }
}
