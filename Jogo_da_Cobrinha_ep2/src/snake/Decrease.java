package snake;

import javax.swing.ImageIcon;

import snake.Campo.Tela_do_Jogo;

public class Decrease extends Simple {
	@Override
    protected void CarregarImagens(){
        ImageIcon imagem = new ImageIcon("src/snake/decrease.png");
        setImage(imagem.getImage());
    }
	
	public void Comido(Tela_do_Jogo tela_do_jogo, Comum cobra) {
		cobra.setCobra(3);
	}
}
