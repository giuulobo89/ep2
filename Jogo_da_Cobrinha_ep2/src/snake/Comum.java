package snake;

import javax.swing.*;
import java.awt.*;

public class Comum {
	protected final int cobra_max = 2500;
	protected final int x[] = new int[cobra_max];
	protected final int y[] = new int[cobra_max];
	protected final int tamanho_da_cabeca = 10;
	
	private int cobra;
	private int pontos;
	
	private boolean esquerda = false;
	private boolean direita = true;
	private boolean cima = false;
	private boolean baixo = false;
	private Image corpo;
	
	public Comum() {
		CarregarImagens();
		Colisao();
		Movimentos();
		setCobra(3);
		setPontuacao(0);
		for(int a = 0; a < getCobra(); a++) {
			getX()[a] = 50 - a * 10;
			getY()[a] = 50;
		}	    
	}

	protected void CarregarImagens() {
		ImageIcon imagem = new ImageIcon("src/snake/comum.png");
	    setCorpo(imagem.getImage());	
	}

	protected boolean Colisao_Barreira(Barreira barreira1, Barreira barreira2, Barreira barreira3) {
		for(int i = 0; i < 25; i++){
            if(this.getX()[0] == barreira1.getX()[i] && this.getY()[0] == barreira1.getY()[i]){
                return false;
            }
        }
        for(int i = 0; i < 25; i++){
            if(this.getX()[0] == barreira2.getX()[i] && this.getY()[0] == barreira2.getY()[i]){
                return false;
            }
        }
        for(int i = 0; i < 10; i++){
            if(this.getX()[0] == barreira3.getX()[i] && this.getY()[0] == barreira3.getY()[i]){
                return false;
            }
        }
        return true;	
	}
	
	public boolean isEsquerda() {
        return esquerda;
    }

    public void setEsquerda(boolean esquerda) {
        this.esquerda = esquerda;
    }

    public boolean isDireita() {
        return direita;
    }

    public void setDireita(boolean direita) {
        this.direita = direita;
    }

    public boolean isCima() {
        return cima;
    }

    public void setCima(boolean cima) {
        this.cima = cima;
    }

    public boolean isBaixo() {
        return baixo;
    }

    public void setBaixo(boolean baixo) {
        this.baixo = baixo;
    }

    public void Placar(){
        this.setPontuacao(this.getPontuacao()+1);

    }

	public void Crescer(){
        this.setCobra(this.getCobra()+1);
    }

	protected void Movimentos() {
        for (int z = tamanho_da_cabeca; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (esquerda) {
            x[0] -= tamanho_da_cabeca;
        }

        if (direita) {
            x[0] += tamanho_da_cabeca;
        }

        if (cima) {
            y[0] -= tamanho_da_cabeca;
        }

        if (baixo) {
            y[0] += tamanho_da_cabeca;
        }
	}

	protected int[] getY() {
		return y;
	}

	protected int[] getX() {
		return x;
	}

	protected int getCobra() {
		return cobra;
	}
	
	protected void setCobra(int cobra) {
		this.cobra = cobra;
	}
	
	protected void setPontuacao(int pontos) {
		this.pontos = pontos;
	}
	
	protected int getPontuacao() {
		return pontos;
	}

    public void setCorpo(Image corpo) {
        this.corpo = corpo;
    }

    public Image getCorpo() {
        return corpo;
    }

	protected boolean Colisao() {
        for (int z = pontos; z > 0; z--) {
        	
            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                return false;
            }
        }

        if (y[0] >= 683) {
            return false;
        }

        if (y[0] < 0) {
            return false;
        }

        if (x[0] >= 687) {
            return false;
        }

        if (x[0] < 0) {
            return false;
        }
        return true;
	}
}