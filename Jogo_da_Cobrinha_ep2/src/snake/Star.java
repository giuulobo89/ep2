package snake;

import javax.swing.*;

public class Star extends Comum {

	@Override
    protected void CarregarImagens() {
        ImageIcon imagem = new ImageIcon("src/snake/star.png");
        this.setCorpo(imagem.getImage());
    }
	
	@Override
    public void Placar(){
        this.setPontuacao(this.getPontuacao()+2);
    }

}
