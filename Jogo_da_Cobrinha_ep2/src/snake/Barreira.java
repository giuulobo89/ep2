package snake;

import javax.swing.*;
import java.awt.*;

public class Barreira {
	private Image imagem;
	public int x[] = new int[25];
    public int y[] = new int[25];
    
    public Barreira(){
        imagem = new ImageIcon("src/snake/barrier.png").getImage();
    }

    public Image getImage() {
        return imagem;
    }
    
    public int[] getX() {
    	return x;
    }
    
    public int[] getY() {
    	return y;
    }
    
    public void setX (int i, int pix){
        x[i] = pix;
    }
    
    public void setY (int i, int pix){
        y[i] = pix;
    }
}
