package snake;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

import java.util.Random;

@SuppressWarnings("serial")
public class Campo extends JFrame {

	private Comum cobra;
	private Simple simple;
	public boolean no_jogo;
	private final JLabel lblJogoDaCobrinha = new JLabel("Jogo da Cobrinha - Snake");
	
	public Campo() {
		MinhaInterface();
	}
		
	public void MinhaInterface() {
		setTitle("Cobra da Giu");
		setSize(683,687);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new GridLayout(3,1));
		getContentPane().setBackground(Color.pink);
		lblJogoDaCobrinha.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblJogoDaCobrinha.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblJogoDaCobrinha);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setBackground(Color.pink);
		panel.setLayout(new GridLayout(3,1));
		
		JButton btnCobraComum = new JButton("Cobra Comum");
		panel.add(btnCobraComum);
		
		JButton btnCobraKitty = new JButton("Cobra Kitty");
		panel.add(btnCobraKitty);
		
		JButton btnCobraStar = new JButton("Cobra Star");
		panel.add(btnCobraStar);
		
		JLabel lblNewLabel = new JLabel("Escolha o tipo de cobra com o qual deseja jogar");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 20));
		getContentPane().add(lblNewLabel);
		setFocusable(true);
		
		  btnCobraComum.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				cobra = new Comum();
	            Tela_do_Jogo tela_do_jogo = new Tela_do_Jogo();
	            setFocusable(true);
	            setContentPane(tela_do_jogo);
			}
		  });

	      btnCobraStar.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent actionEvent) {
	                cobra = new Star();
	                Tela_do_Jogo tela_do_jogo = new Tela_do_Jogo();
	                setFocusable(true);
	                setContentPane(tela_do_jogo);
	            }
	       });

	        btnCobraKitty.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent actionEvent) {
	                cobra = new Kitty();
	                Tela_do_Jogo tela_do_jogo = new Tela_do_Jogo();
	                setFocusable(true);
	                setContentPane(tela_do_jogo);
	            }
	        });
	        
	        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public class Tela_do_Jogo extends JPanel implements ActionListener {
		private Barreira barreira1;
        private Barreira barreira2;
        private Barreira barreira3;

        private Simple good_fruit;
        private Timer timer;
        public boolean no_jogo;
        @SuppressWarnings("unused")
		private GerarFrutinha gerar_frutinha;

        private InputMap im = getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
        private ActionMap am = this.getActionMap();

        public Tela_do_Jogo(){
            iniciarTela();
        }

		@Override
		public void actionPerformed(ActionEvent actioEvent) {
	         if(no_jogo){

	             Colisao();
	             cobra.Movimentos();
	             VerificarComida();
	         }

	         repaint();
		}
	
	private void iniciarTela(){
		
        setFocusable(true);
        requestFocusInWindow();
        no_jogo = true;
        setSize(new Dimension(683, 687));
        setVisible(true);
        iniciarJogo();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "Seta_para_direita");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "Seta_para_esquerda");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "Seta_para_cima");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "Seta_para_baixo");

        am.put("Seta_para_direita", new ArrowAction("Seta_para_direita"));
        am.put("Seta_para_esquerda", new ArrowAction("Seta_para_esquerda"));
        am.put("Seta_para_cima", new ArrowAction("Seta_para_cima"));
        am.put("Seta_para_baixo", new ArrowAction("Seta_para_baixo"));
    }
	
	private void iniciarJogo(){
		
		good_fruit = new Simple();
		
		gerar_frutinha = new GerarFrutinha();
		
        @SuppressWarnings("unused")
		int random = (int)Math.random() * 2;

        good_fruit = new Simple();
        barreira1 = new Barreira();
        for(int i=0; i < 25; i++) {
            barreira1.setX(i, 80);
            barreira1.setY(i, 120 + i * 10);
        }
        barreira2 = new Barreira();
        for(int i=0; i < 25; i++) {
            barreira2.setX(i, 410);
            barreira2.setY(i, 120 + i * 10);
        }

        barreira3 = new Barreira();
        for(int i=0; i < 10; i++) {
            barreira3.setX(i, 200 + i * 10);
            barreira3.setY(i, 250);
        }
        while (good_fruit.na_Barreira(barreira1, barreira2, barreira3)) {
            good_fruit.localizar_comida();
        }

        timer = new Timer(70, this);
        timer.start();
    }
	
	public void paintComponent(Graphics g){
        super.paintComponents(g);
        doDrawing(g);
    }
	
	private void doDrawing(Graphics g) {
        if (no_jogo) {

            g.setColor(Color.black);
            g.fillRect(0,0,800,600);
            String mensagem = "Pontos: " + cobra.getPontuacao();
            Font big = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metr = getFontMetrics(big);

            g.setColor(Color.white);
            g.setFont(big);
            g.drawString(mensagem, (500 - metr.stringWidth(mensagem)) / 2, 50);

            g.drawImage(good_fruit.getImage(), good_fruit.getX(), good_fruit.getY(), this);
           // g.drawImage(bad_fruit.getImage(), bad_fruit.getX(), bad_fruit.getY(), this);

            for(int i=0; i < 25; i++) {
                g.drawImage(barreira1.getImage(), barreira1.getX()[i], barreira1.getY()[i], this);
            }
            for(int i=0; i<25; i++) {
                g.drawImage(barreira2.getImage(), barreira2.getX()[i], barreira2.getY()[i], this);
            }
            for(int i=0; i < 10; i++) {
                g.drawImage(barreira3.getImage(), barreira3.getX()[i], barreira3.getY()[i], this);
            }
            for (int i = 0; i < cobra.getCobra(); i++) {
                g.drawImage(cobra.getCorpo(), cobra.getX()[i], cobra.getY()[i], this);
            }
            Toolkit.getDefaultToolkit().sync();

        } else {
            gameOver(g);
        }
    }
	
	 private void gameOver(Graphics g){
         String mensagem = "Game Over";
         Font small = new Font("Helvetica", Font.BOLD, 30);
         FontMetrics metrica = getFontMetrics(small);

         g.setColor(Color.red);
         g.setFont(small);
         g.drawString(mensagem, (683 - metrica.stringWidth(mensagem)) / 2, 687 / 2);
     }

     private void Colisao(){
         no_jogo = cobra.Colisao() && cobra.Colisao_Barreira(barreira1, barreira2, barreira3) ;
         if(!no_jogo){
        	 timer.stop();
         }
     }
     
     private void VerificarComida() {
    	 
         if (good_fruit.getX() == cobra.getX()[0] && good_fruit.getY() == cobra.getY()[0]) {
             good_fruit.Comido(this, cobra);
             Random b = new Random();
             int random = b.nextInt(4);
             if(random == 0) {
                 good_fruit = new Simple();
             }
             else if(random == 1){
                 good_fruit = new Big();
             }
             else if(random == 2) {
            	 good_fruit = new Decrease();
             }
             else if(random == 3) {
                 good_fruit = new Bomb();
             }
         }
         
     }
     
     private class GerarFrutinha implements ActionListener{
    	 private Timer tempo_da_frutinha;
    	 private final int atraso = 10000;
    	 public GerarFrutinha() {
    		 tempo_da_frutinha = new Timer(atraso, this);
    		 tempo_da_frutinha.start();
    	 }
		@Override
		public void actionPerformed(ActionEvent e) {
			Random b = new Random();
            int random = b.nextInt(4);
            if(random == 0) {
                good_fruit = new Simple();
            }
            else if(random == 1){
                good_fruit = new Big();
            }
            else if(random == 2) {
           	 good_fruit = new Decrease();
            }
            else if(random == 3) {
                good_fruit = new Bomb();
            }
        }
    	 
     }
     
     public class ArrowAction extends AbstractAction {

         private String comando;

         public ArrowAction(String comando) {
             this.comando = comando;
         }

         @Override
         public void actionPerformed(ActionEvent e) {
             if (comando.equalsIgnoreCase("Seta_para_esquerda") && (!cobra.isDireita())) {
                 cobra.setEsquerda(true);
                 cobra.setCima(false);
                 cobra.setBaixo(false);
             } else if (comando.equalsIgnoreCase("Seta_para_direita") && (!cobra.isEsquerda())) {
                 cobra.setDireita(true);
                 cobra.setCima(false);
                 cobra.setBaixo(false);
             } else if (comando.equalsIgnoreCase("Seta_para_cima") && (!cobra.isBaixo())) {
                 cobra.setCima(true);
                 cobra.setDireita(false);
                 cobra.setEsquerda(false);
             } else if (comando.equalsIgnoreCase("Seta_para_baixo") && (!cobra.isCima())) {
                 cobra.setBaixo(true);
                 cobra.setDireita(false);
                 cobra.setEsquerda(false);
             }
         }
     }

	}
}